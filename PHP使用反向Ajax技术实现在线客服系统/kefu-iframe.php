<?php
/**
 * 通过iframe来实现反向Ajax
 * @author webbc
 */
header('Content-type:text/html;charset=utf-8');
set_time_limit(0);//设置页面永久执行
//ob_start();//打开输出缓存过了，就不需要使用此函数了
//由于浏览器是根据内容大小才先显示，可以先显示4000个空白字符串让浏览器可以继续显示
echo str_repeat(' ', 4000),"<br/>";
ob_flush();
flush();
while(true){
  //从数据库读取一条未读的咨询消息
  require('./conn.php');
  $sql = "select * from kefu_msg where rec = 'admin' and isread = 0 limit 0,1";
  $result = mysqli_query($conn,$sql);
  $msg = mysqli_fetch_assoc($result);
  //如果有消息
  if(!empty($msg)){
    //设置该消息为已读
    $sql = 'update kefu_msg set isread = 1 where mid = '.$msg['mid'];
    mysqli_query($conn,$sql);
    $json = json_encode($msg);//把数组转换为json数据
    //返回js脚本，影响iframe的父窗体
    echo '<script>';
    echo 'parent.window.comet(',$json,');';
    echo '</script>';
    ob_flush();//强制让php返回给apache
    flush();//强制让web服务器返回给浏览器
  }
  sleep(1);//隔1s循环查1次
}