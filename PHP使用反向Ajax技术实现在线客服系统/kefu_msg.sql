/*
Navicat MySQL Data Transfer

Source Server         : localhost
Source Server Version : 50553
Source Host           : localhost:3306
Source Database       : test

Target Server Type    : MYSQL
Target Server Version : 50553
File Encoding         : 65001

Date: 2019-09-28 10:50:34
*/

SET FOREIGN_KEY_CHECKS=0;

-- ----------------------------
-- Table structure for kefu_msg
-- ----------------------------
DROP TABLE IF EXISTS `kefu_msg`;
CREATE TABLE `kefu_msg` (
  `mid` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `pos` varchar(100) NOT NULL COMMENT '发送人',
  `rec` varchar(100) NOT NULL COMMENT '接收人',
  `content` text NOT NULL COMMENT '资讯/回复内容',
  `isread` tinyint(1) NOT NULL DEFAULT '0' COMMENT '是否已读 0未读 1已读',
  `time` varchar(11) NOT NULL COMMENT '时间',
  PRIMARY KEY (`mid`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COMMENT='客服信息表';
