一、数据库文件：
kefu_msg.sql

二、文件列表：
kefu-amdin.php ———— 客服人员界面
kefu-ajax.php  ———— 咨询人请求回复信息
kefu-iframe.php ———— 客服人请求咨询信息
kefu-sendmag.php ———— 发送回复/咨询信息
kefu-user.php ———— 咨询人员界面
conn.php ———— 数据库连接文件

三、各文件功能说明：
1、客服人员界面（kefu-amdin.php）：

此处采用长连接。页面中主要有一个div，用于显示聊天信息，还有一个隐藏的iframe标签，这个iframe实现反向Ajax模型，用于发送长时连接，当服务器有数据时，服务器将调用comet()方法，此方法显示咨询内容，choose()方法是选择咨询人，resp()是回复方法，在这里会向16-kefu-sendmsg.php页面发出ajax请求，向数据库插入一条回复信息，回复成功后并显示到聊天窗口中。

2、发送咨询/回复消息（kefu-sendmsg.php）：

主要是接受信息，把数据写入到数据库中

3、客服人请求咨询信息（kefu-iframe.php）：

主要功能是保持连接永不断开，然后不断的从数据库读取一条未读的咨询消息，如果有消息，先设置该消息为已读，返回js脚本，影响iframe的父窗体

4、咨询人员界面（kefu-user.php）：

此处采用长连接+长轮询的方式。当页面加载就发出一条ajax请求，如果该请求有数据返回，则显示到聊天窗口中，延时1s后重新发送请求，如果点击咨询，就发出ajax请求将咨询内容写入数据库中。

5、咨询人请求回复信息界面（kefu-ajax.php）：

通过ajax+长轮询实现反向Ajax。请求数据，获取数据后，将数据置为已读，然后返回，结束本次连接。

6、数据库连接文件（conn.php）：
连接数据库

四、链接地址：
https://mp.weixin.qq.com/s/RZqAVN2EE9yFEdcV-7FLpw

