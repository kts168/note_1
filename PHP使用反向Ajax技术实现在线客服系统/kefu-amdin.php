<?php
setcookie('username','admin');
?>
<!DOCTYPE html>
<html>
<head>
  <meta charset="utf-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <title>客服功能——客服人员端</title>
  <link rel="stylesheet" href="">
<script>
  var xhr = new XMLHttpRequest();
  //服务器调用函数
  function comet(json){
    var content = '<p style="text-align:left"><span onclick="choose(\''+ json.pos +'\');">' + json.pos + '</span>说：'+json.content+'</p>';
    var old = document.getElementById('chatArea').innerHTML;
    document.getElementById('chatArea').innerHTML = old + content;
  }
  //咨询人选择函数
  function choose(pos){
    document.getElementById('postman').innerHTML = pos;
  }
  //客服人员回复函数
  function resp(){
    var respContent = document.getElementById('respContent').value;
    var pos = document.getElementById('postman').innerHTML;
    if(respContent == '' || pos == ''){
      alert('请重新选择回复人或填写回复内容');
      return;
    }
    //ajax提交请求
    xhr.open('POST','kefu-sendmsg.php',true);
    xhr.setRequestHeader('Content-Type','application/x-www-form-urlencoded');
    xhr.onreadystatechange = function (){
      if(this.readyState == 4 && this.status == 200){
        if(this.responseText == 'ok'){
          //回复成功，把回复信息显示到聊天界面中
          var content = '<p style="text-align:right">你回复'+ pos + '：'+respContent+'</p>';
          var old = document.getElementById('chatArea').innerHTML;
          document.getElementById('chatArea').innerHTML = old + content;
          document.getElementById('respContent').value = '';//给回复内容重新置空
        }
      }
    }
    var sendData = 'rec=' + pos + '&content='+respContent;
    xhr.send(sendData);
  }  
</script>
<style>
  #chatArea{
    width:500px;
    height:400px;
    border:1px solid black;
    overflow: scroll;
  }
</style>
</head>
<body>
  <h1>客服功能——客服人员端</h1>
  <h2>原理：iframe+长连接</h2>
  <div id="chatArea">
  </div>
  <iframe width="0" height="0" frameborder="0" name="frame" src="./kefu-iframe.php"></iframe>
  <p>咨询人：<span id="postman"></span></p>
  <p><textarea id="respContent"></textarea></p>
  <p><input type="button" value="回复" onclick="resp();" /></p>
</body>
</html>