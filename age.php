<?php
/**
 * php计算年龄
 */
date_default_timezone_set('PRC');
$bday = new DateTime('15.9.1993'); // 你的出生日
$today = new Datetime(date('m.d.y'));
$diff = $today->diff($bday);
// var_dump($diff);
printf(' 你的年龄是 : %d 岁, %d 月, %d 天', $diff->y, $diff->m, $diff->d);
printf("\n");
/*其中printf() 函数表示输出格式化的字符串，也就是规定字符串以及如何格式化其中的变量。

其中可能的格式值：

%% - 返回一个百分号 %

%b - 二进制数

%c - ASCII 值对应的字符

%d - 包含正负号的十进制数（负数、0、正数）

%e - 使用小写的科学计数法（例如 1.2e+2）

%E - 使用大写的科学计数法（例如 1.2E+2）

%u - 不包含正负号的十进制数（大于等于 0）

%f - 浮点数（本地设置）

%F - 浮点数（非本地设置）

%g - 较短的 %e 和 %f

%G - 较短的 %E 和 %f

%o - 八进制数

%s - 字符串

%x - 十六进制数（小写字母）

%X - 十六进制数（大写字母）
*/