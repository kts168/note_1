<?php
/**
 * php输出html源代码，可以通过htmlspecialchars 函数来实现。htmlspecialchars 表示将特殊字符转换为HTML实体。
 */
// file() 函数把整个文件读入一个数组中。
// 数组中的每个元素都是文件中相应的一行，包括换行符在内。
/*
echo '<pre>';
print_r(file('notice.txt'));
*/
$all_lines = file('http://www.php.cn/');
foreach ($all_lines as $line_num => $line)
{
    echo "第{$line_num}行: " . htmlspecialchars($line) . "<br>";
}
