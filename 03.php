<?php

/**
 * PHP 统计字符重复次数和字符
*/
 
echo '<pre>';
$str = 'aaaaabbbbbcccss'; // 字符串示例
echo $str . '<br/>';
// 把出现过的字符记录在此数组中，如果记录有，则不记录
// 
$strRecord = array();
for ($i = 0; $i < strlen($str); $i++) {
    //默认设置为没有遇到过
    $found = 0;
    foreach ($strRecord as $k => $v) {
        if ($str[$i] == $v['key']) {
            //记录再次遇到的字符，count + 1；
            $strRecord[$k]['count'] += 1;
            //设置已经遇到过的，标记
            $found = 1;
            //如果已经遇到，不用再循环记录数组了，继续下一个字符串比较
            break;
        }
    }
    if (!$found) {
        //记录第一次遇到的字符，count + 1；
        $strRecord[] = array('key' => $str[$i], 'count' => 1);
    }
}
 
print_r($strRecord);
$str2 = '';
foreach ($strRecord as $k => $v) {
    foreach ($v as $key => $value) {
        $str2 .= $value;
    }
 
}
echo $str2;