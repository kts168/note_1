<?php
/**
 * PHP实现简易汉字验证码的思路
 */
// 创建画布
$image = imagecreatetruecolor(200, 60);
$background = imagecolorallocate($image, 255, 255, 255);
imagefill($image, 0, 0, $background);

// 画干扰点
for ($i=0; $i < 300; $i++) {
    $pixColor = imagecolorallocate($image, rand(150, 240), rand(150, 240), rand(150, 240));
    $pixX = rand(10, 190);
    $pixY = rand(5, 55);
    imagesetpixel($image, $pixX, $pixY, $pixColor);
}

// 画干扰线
//4条水平线
for ($i=0; $i < 5; $i++) {
    $lineColor = imagecolorallocate($image, rand(50, 150), rand(50, 150), rand(50, 150));
    $lineX1 = 0;
    $lineX2 = 300;
    $lineY1 = ($i + 1) * 12;
    $lineY2 = ($i + 1) * 12;
    imageline($image, $lineX1, $lineY1, $lineX2, $lineY2, $lineColor);
}

//10条垂直线
for ($i=0; $i < 30; $i++) {
    $lineColor = imagecolorallocate($image, rand(50, 150), rand(50, 150), rand(50, 150));
    $lineX1 = ($i + 1) * 10;
    $lineX2 = ($i + 1) * 10;
    $lineY1 = 0;
    $lineY2 = 60;
    imageline($image, $lineX1, $lineY1, $lineX2, $lineY2, $lineColor);
}
// 画汉字
$text = array('栀', '子', '花', '开');
for ($i=0; $i < 4; $i++) {
    $textColor = imagecolorallocate($image, rand(20, 100), rand(20, 100), rand(20, 100));
    $textX = $i * 50 + 10;
    $textY = rand(40, 60);
    // 字体文件为英文名
    imagettftext($image, 30, rand(20, 50), $textX, $textY, $textColor, "STFANGSO.TTF", $text[$i]);
}
// 输出图像
header("content-type:image/png");
imagepng($image);
// 销毁图像
imagedestroy($image);