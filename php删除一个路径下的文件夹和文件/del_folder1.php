<?php
/*
	删除一个路径下的文件夹和文件
	
	unlink($filename) 删除文件。

	rmdir($path) 只删除空文件夹
 */
 // 方法1
 // 无限循环和迭代
$path = "./aa";
function deleteDir($dir)
{
    if (!$handle = @opendir($dir)) {
        return false;
    }
    while (false !== ($file = readdir($handle))) {
        if ($file !== "." && $file !== "..") { //排除当前目录与父级目录
            $file = $dir . '/' . $file;
            if (is_dir($file)) {
                deleteDir($file);
            } else {
                @unlink($file);
            }
        }
    }
    @rmdir($dir);
}
deleteDir($path);