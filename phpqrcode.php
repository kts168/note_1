<?php
header('content-type:text/html;charset=utf-8');
/**
        PHP QR Code官网
        https://sourceforge.net/projects/phpqrcode/
        下载扩展，解压后放在项目目录下即可
*/
// 引用类库文件
require_once 'lib/phpqrcode/phpqrcode.php';
$url='https://www.baidu.com/';
$errorCorrectionLevel = 'L';//容错级别
$matrixPointSize = 6;//生成图片大小
//生成二维码图片
$imgname = 'baidu.png';
\QRcode::png($url, $imgname, $errorCorrectionLevel, $matrixPointSize, 2);

echo '<p><h3>二维码图片</h3><br/><img src="'.$imgname.'"></p>';
$logo = 'logo.png'; // logo图片
$QR = 'baidu.png';  // 已生成的二维码图片
if ($logo !== FALSE) {
	$QR = imagecreatefromstring(file_get_contents($QR));
	$logo = imagecreatefromstring(file_get_contents($logo));
	$QR_width = imagesx($QR);//二维码图片宽度
	$QR_height = imagesy($QR);//二维码图片高度
	$logo_width = imagesx($logo);//logo图片宽度
	$logo_height = imagesy($logo);//logo图片高度
	$logo_qr_width = $QR_width / 5;
	$scale = $logo_width/$logo_qr_width;
	$logo_qr_height = $logo_height/$scale;
	$from_width = ($QR_width - $logo_qr_width) / 2;
	//重新组合图片并调整大小
	imagecopyresampled($QR, $logo, $from_width, $from_width, 0, 0, $logo_qr_width,
	$logo_qr_height, $logo_width, $logo_height);
}
//输出图片
imagepng($QR, 'baidu_logo.png');
echo '<p><h3>带LOGO的二维码图片</h3><br/><img src="baidu_logo.png"></p>';