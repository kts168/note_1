<?php
/**
 * 缓存
 */
class Cache {
    public function index($options) {
        //判断文件是否存在，判断是否过期
        if (is_file('shtml/index.shtml') && (time() - filemtime('shtml/index.shtml') < 300)) {
            require_once ('shtml/index.shtml');
        }else {
            require_once ('Db.php');
            $con = Db::getInstance($options)->db();
            $sql = "SELECT * FROM pro_test";
            $exe_res = mysqli_query($con, $sql);
            $res = mysqli_fetch_all($exe_res);
            try{
                if (!$res) {
                    throw new Exception("no result");
                }
            }catch (Exception $e) {
                echo 'Message: ' .$e->getMessage();
            }
            //开启缓存区，这后面的内容都会进缓存区
            ob_start();
            //引入模板文件(模板会渲染数据)
            require_once ('templates/index.php');
            //取出缓存区内容（在这里是渲染后的模板），将其保存（默认会覆盖原来的）为index.shtml（static html）
            file_put_contents('shtml/index.shtml', ob_get_contents());
        }
    }
}
//数据库配置信息
$options = [
  'db_host' => 'localhost',
  'db_user' => 'root',
  'db_password' => '123456',
  'db_database' => 'test'
];
$obj = new Cache();
$obj->index($options);