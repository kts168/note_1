<?php
/*
	PHP 正则验证
 */
/**
 * 检查手机号码格式
 * @param $mobile 手机号码
 */
function check_mobile($mobile){
    if(preg_match('/1[345678]\d{9}$/',$mobile))
        return true;
    return false;
}

/**
 * 检查固定电话
 * @param $mobile
 * @return bool
 */
function check_telephone($mobile){
    if(preg_match('/^([0-9]{3,4}-)?[0-9]{7,8}$/',$mobile))
        return true;
    return false;
}
/**
 * 检查数字
 * @param  $number
 * @return bool
 */
function check_number($number){
	if(preg_match('/^\d*$/',$number))
		return true;
	return false;
}
/**
 * 检查邮箱
 * @param  $email
 * @return bool
 */
function check_email($email){
	if(preg_match('/([a-z0-9]*[-_.]?[a-z0-9]+)*@([a-z0-9]*[-_]?[a-z0-9]+)+[.][a-z]{2,3}([.][a-z]{2})?/i',$email))
		return true;
	return false;
}
/**
 * 检查密码(必须包含数字和字母)
 * @param  $psd
 * @return bool
 */
function check_psd($psd){
	if(preg_match('/^(?![0-9]+$)(?![a-zA-Z]+$)[0-9A-Za-z]{6,10}$/',$psd))
		return true;
	return false;
}