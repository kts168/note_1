<?php
/**
 * PHP 读取 excel 文件内容、获取单元格数据
 *
 * memory_get_usage()  php内置函数能返回当前分配给PHP脚本的内存量，使用它来调试PHP代码性能
 */
date_default_timezone_set('PRC');
/**
 * 获取excal文件数据
 * @param  string $extend 扩展文件路径
 * @param  string $filename 文件名
 * @return array
 */
function getExcelData($extend,$filename){
	if(!file_exists($filename)){
		exit('文件'.$filename.'不存在！');
	}
	$startTime = time(); //返回当前时间的时间戳
	require_once $extend;
	$objPHPExcel = PHPExcel_IOFactory::load($filename);
	//获取sheet表格数目
	$sheetCount = $objPHPExcel->getSheetCount();
	//默认选中sheet0表
	$sheetSelected = 0;
	$objPHPExcel->setActiveSheetIndex($sheetSelected);
	//获取表格行数
	$rowCount = $objPHPExcel->getActiveSheet()->getHighestRow();
	//获取表格列数
	$columnCount = $objPHPExcel->getActiveSheet()->getHighestColumn();

	/* 循环读取每个单元格的数据 */
	//行数循环
	for ($row = 1; $row <= $rowCount; $row++){
		//列数循环 , 列数是以A列开始
		for ($column = 'A'; $column <= $columnCount; $column++) {
		    $dataArr[$row][] = $objPHPExcel->getActiveSheet()->getCell($column.$row)->getValue();
		}
		// echo "<br/>消耗的内存为：".(memory_get_peak_usage(true) / 1024 / 1024)."M".'<br/>';
			
	}
	return $dataArr;
}

$path = './PHPExcel/PHPExcel/IOFactory.php';
$filename = 'demo.xls';
$data = getExcelData($path,$filename);
echo '<pre>';
print_r($data);
