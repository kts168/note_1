<?php
// 将CSV文件转换为PHP多维数组
// 
$filename = 'example.csv';
// 用于保存所有数据的多维（嵌套）数组
$the_big_array = [];

// 打开文件并阅读
if (($h = fopen("{$filename}", "r")) !== FALSE)
{
	// 文件中的每一行数据都被转换为我们调用的单个数组$data
	// 数组的每个元素以逗号分隔
	while (($data = fgetcsv($h, 1000, ",")) !== FALSE)
	{
	    // 每个单独的数组都被存入到嵌套的数组中
	   	$the_big_array[] = $data;
	}
	// 关闭文件
	fclose($h);
}
// 以可读格式显示代码
echo "<pre>";
var_dump($the_big_array);
echo "</pre>";