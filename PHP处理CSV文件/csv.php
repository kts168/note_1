<?php
/**
 * 如何使用PHP处理CSV文件？

	php解析CSV文件可以使用fgetcsv()函数；该函数从打开的文件中解析一行，校验 CSV 字段。
 */

/**
 * 语法如下：

	fgetcsv("filename.csv", 1000, ",");

	filename.csv：CSV文件的名称。

	1000：表示最长线的长度。

	“，”：表示可选的分隔符参数。默认当然是逗号（，）。
 */
/**
 * csv文件转换为数组
 * @param  string $filename csv文件
 * @return array           转换后的数组
 */
function csvToarr($filename){
	// 打开文件并阅读
	if (($h = fopen("{$filename}", "r")) !== FALSE)
	{
	  	// 将每行数据都转换为本地$data变量并储存
	  	while (($data = fgetcsv($h, 1000, ",")) !== FALSE)
	  	{
	   		//输出每行数据
	    	var_dump($data);
	  	}
	  	// 关闭文件
	  	fclose($h);
	}
}
$filename = 'example.csv';
csvToarr($filename);