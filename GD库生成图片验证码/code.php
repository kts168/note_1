<?php
    //启动session
    session_start();
    $code = "";         //验证码字符串
    $str = "qwertyuiopasdfghjklzxcvbnm1234567890";  //验证码字符取值范围[a-z0-9]
    $w = 160;           //图片宽度
    $h = 40;            //图片高度
    $num = 4;           //验证码字符数
    $dotNum = 300;      //干扰点个数
    $lineNum = rand(3, 5);         //干扰线条数
    $font = "simsunb.ttf";     //设置字体文件
    $image = imagecreatetruecolor($w, $h);  //创建一张指定宽高的图片
    $imageColor = imagecolorallocate($image, 255, 255, 255);   //设置背景图片颜色为白色
    imagefill($image, 0, 0, $imageColor);  //填充图片背景
    //随机验证码，包含字母和数字
    for ($i = 0; $i < $num; $i++) {
        $fontColor = imagecolorallocate($image, rand(0, 120), rand(0, 120), rand(0, 120));  //生成随机字体颜色
        $content = substr($str, rand(0, strlen($str)), 1);      //随机取字符集中的值
        $code .= $content;
        $fontSize = rand(15, 25);                    //字体大小
        $x = $i * $w / $num + rand(5, 10);          //指定生成位置X轴偏移量
        $y = rand(20, 30);                          //指定生成位置Y轴偏移量
        imagettftext($image, $fontSize, 0, $x, $y, $fontColor, $font, $content);
    }
    $_SESSION["code"] = $code;  //保存验证码字符串到session中
    //生成干扰点
    for ($i = 0; $i < $dotNum; $i++) {
        $dotColor = imagecolorallocate($image, rand(0, 255), rand(0, 255), rand(0, 255));
        imagesetpixel($image, rand(0, $w), rand(0, $h), $dotColor);
    }

    //生成干扰线
    for ($i = 0; $i < $lineNum; $i++) {
        $lineColor = imagecolorallocate($image, rand(0, 100), rand(0, 100), rand(0, 100));

        imageline($image, rand(0, $w), rand(0, $h), rand(0, $w), rand(0, $h), $lineColor);
    }
    header("content-type:image/png");
    imagepng($image);
    imagedestroy($image);