<?php
    if(isset($_REQUEST["code"])){
        session_start();
        if(strtolower($_POST["code"])==$_SESSION["code"]){
            echo "<script>alert('正确！')</script>";
        }else{
            echo "<script>alert('错误！')</script>";
        }
    }
?>
<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>验证码</title>
    <style>
        #code{
            border: 1px solid #ccc;
            vertical-align: bottom;
        }
        #refresh{
            text-decoration: none;
            font-size: .875em;
        }
    </style>
</head>
<body>
    <form action="" method="post">
        <p>
            验证码：
            <img src="code.php?r=<?php echo rand()?>" alt="" id="code">
            <a href="javascript:;" id="refresh">看不清？</a>
        </p>
        <p>
            输入验证码：
            <input type="text" name="code">
        </p>
        <input type="submit" value="提交">
        <script>
            document.getElementById("code").onclick = document.getElementById("refresh").onclick = refresh;
            function refresh() {
                document.getElementById('code').src='code.php?r='+Math.random()
            }
        </script>
    </form>
</body>
</html>