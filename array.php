<?php
/**
 * 如何比较两个PHP多维数组
 */
$color1 = array(array('Red',80),array('Green',70),array('white',60));

$color2 = array(array('Green',70),array('Black',95));

$color = array_udiff($color1,$color2,create_function(
	'$a,$b','return strcmp(implode("",$a),implode("",$b));')
);

echo "<pre>";

print_r($color);

/**
 * 1、array_udiff ： 用回调函数比较数据来计算数组的差集
 * 
 * 2、create_function：创建一个匿名函数
 * 	create_function （ string $args ， string $code ）： string
 * 3、strcmp ：二进制安全字符串比较
 * 4、implode：将一个一维数组的值转化为字符串
 *
 * 
 */
// 参考链接
//https://mp.weixin.qq.com/s?__biz=MzU2MDcwNjE0Nw==&mid=2247485236&idx=2&sn=eb7b2e8cea4126fbc6d87141d95c309a&chksm=fc02a038cb75292e14b2a0a35ef5fc0f3324b2efede3e2c49a43a20ffe3459125d2510b43029&mpshare=1&scene=1&srcid=0119w0Z7K7dopkMdEHATvfhv#rd
//
//
//	PHP数组函数实现栈与队列的方法介绍
//	
//	array_push(),array_pop(),array_unshift(),array_shift()配合数组本身,一下子就实现了栈(stack)和队例(quene)
//	
//	定义一个栈，直接就是一个$rangelist=array("战狼","战狼2","流浪地球")，操作他，入栈array_push($rangelist,"吴京")，出栈$result=array_pop($rangelist)，出栈元素直接到$result中
//	
//	定义一个队列，还是来一个数组，$quenelist=array("战狼","战狼2","流浪地球")，入队array_unshift($quenelist,"红海行动")，出队，$result=array_shift($quenelist),出队的元素存入$result中
//	
//	数组的每个键添加一个字符或多个字符
//	使用array_combine()， array_keys()和array_map()函数来实现在数组的每个键上添加前缀。
//	
$myArray = ['0'=>'Hi','1'=>'Hello','2'=>'Hey'];
$myNewArray = array_combine(
	array_map(function($key){ return 'a'.$key; }, array_keys($myArray)),$myArray
);
print_r($myNewArray);
