<?php
// 素数
// 是大于1的除了1和本身整除的数
// 小于100的素数
// 
$primes = array();
$is_prime_no = false;
for ($i = 2; $i<100; $i++) {
    $is_prime_no = true;
    for ($j = 2; $j<=($i/2); $j++) {
        if ($i%$j==0) {
            $is_prime_no = false;
            break;
        }
    }
    if ($is_prime_no) {
        array_push($primes,$i);
    }
    if (count($primes)==100) {
        break;
    }
}
echo '小于100的素数','<pre>';
print_r($primes);
echo '小于100的素数之和:',array_sum($primes),'<hr/>';
