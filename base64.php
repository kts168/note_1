<?php
/*
	php 实现base64加密解密
	使用PHP自带的函数 base64_encode() 和 base64_decode() 进行加密和解密
 */

$str = 'This is an encoded string';

echo base64_encode($str);

echo '<br>';
$mnum = base64_encode($str);
echo base64_decode($mnum);