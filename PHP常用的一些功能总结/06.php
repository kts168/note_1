<?php
/**
 * 从网上下载文件(支持所有文件类型)
 * @param  string $url url
 * @param  string $path 文件保存路径
 * @return string      文件名(默认保存在当前目录下)
 */
function getUrlContent($url,$path=''){
	set_time_limit(0);
	$pi = pathinfo($url);
	
	$ext = $pi['extension'];

	$name = $pi['filename'];

	// 创建一个新的cURL 资源

	$ch = curl_init();

	// 设置URL和其他必要的参数

	curl_setopt($ch, CURLOPT_URL, $url);

	curl_setopt($ch, CURLOPT_HEADER, false);

	curl_setopt($ch, CURLOPT_BINARYTRANSFER, true);

	curl_setopt($ch, CURLOPT_AUTOREFERER, true);

	curl_setopt($ch, CURLOPT_FOLLOWLOCATION, true);

	curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);

	// 抓取URL并将其传递给浏览器
	$opt = curl_exec($ch);

	// 关闭cURL资源，释放系统资源
	curl_close($ch);

	$saveFile = $name.'.'.$ext;

	if(preg_match("/[0-9a-z._-]/i", $saveFile)){
		if($path){
			$saveFile = $path.md5(microtime(true)).'.'.$ext;
		} else {
			$saveFile = md5(microtime(true)).'.'.$ext;
		}
		$handle = fopen($saveFile, 'wb');

		fwrite($handle, $opt);

		fclose($handle);
		return $saveFile;
	}
}
$url = 'http://www.php.cn/course.html';
$file = getUrlContent($url,'download/');
echo $file;