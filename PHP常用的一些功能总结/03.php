<?php
// <h2>领先的 Web 技术教程 - 全部免费</h2><p>在 W3School，你可以找到你所需要的所有的网站建设教程。</p><p>从基础的 HTML 到 CSS，乃至进阶的 XML、SQL、JS、PHP 和 ASP.NET。</p><p><strong>从左侧的菜单选择你需要的教程！</strong></p><h3>完整的网站技术参考手册</h3><p>我们的参考手册涵盖了网站技术的方方面面。</p>
/*
	strip_tags() 函数剥去字符串中的 HTML、XML 以及 PHP 的标签。
 */
echo strip_tags("领先的 Web 技术教程<h2> - 全部免费</h2>");
echo '<hr/>';
/**
 * 将富文本中文字截取其中的一部分
 * @param $content
 * @return string
 */
function html_substr_content($content,$length=100)
{

    $content = htmlspecialchars_decode($content);      //把一些预定义的 HTML 实体转换为字符

    $content = str_replace(" ", "", $content);     //将空格替换成空

    $content = strip_tags($content);                 //函数剥去字符串中的 HTML、XML 以及 PHP 的标签,获取纯文本内容

    $con = mb_substr($content, 0, $length, "utf-8");   //返回字符串中的前100字符串长度的字符

    return $con;
}
$str = '<h2>sdfdsg领先的 Web 技术教程 - 全部免费</h2><p>在 W3School，你可以找到你所需要的所有的网站建设教程。</p><p>从基础的 HTML 到 CSS，乃至进阶的 XML、SQL、JS、PHP 和 ASP.NET。</p><p><strong>从左侧的菜单选择你需要的教程！</strong></p><h3>完整的网站技术参考手册</h3><p>我们的参考手册涵盖了网站技术的方方面面。</p>';
$content = htmlspecialchars($str);
$rs = html_substr_content($content);
var_dump($rs);