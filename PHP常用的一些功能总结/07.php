<?php
/*
	截取图片
 */
$filename= "b.jpg";

list($w, $h, $type, $attr) = getimagesize($filename);

$src_im = imagecreatefromjpeg($filename);

$src_x = '0'; // 开始 x

$src_y = '0'; // 开始 y

$src_w = '200'; // 宽度

$src_h = '200'; // 高度

$dst_x = '0'; // 结束 x

$dst_y = '0'; // 结束 y

$dst_im = imagecreatetruecolor($src_w, $src_h);

$white = imagecolorallocate($dst_im, 255, 255, 255);

imagefill($dst_im, 0, 0, $white);

imagecopy($dst_im, $src_im, $dst_x, $dst_y, $src_x, $src_y, $src_w, $src_h);

header("Content-type: image/png");

imagepng($dst_im);

imagedestroy($dst_im);