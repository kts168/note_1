<?php
/*
	删除一维数组的空数据
 */
function where_data($data)
{
    foreach ($data as $k => $v) {
        if (empty($v) && $v !='0') {
            unset($data[$k]);
        }
    }
    return $data;
}
$arr = [0,'1','','0',0,23];
$rs = where_data($arr);
echo '<pre/>';
print_r($arr);
print_r($rs);