<?php
/*
	随机颜色生成器
	#+六位字母数字组合
 */
function randomColor() {
	$str = '#';
	for($i = 0 ; $i < 6 ; $i++) {
		$randNum = rand(0 , 15);
		switch ($randNum) {
			case 10: $randNum = 'A'; break;
			case 11: $randNum = 'B'; break;
			case 12: $randNum = 'C'; break;
			case 13: $randNum = 'D'; break;
			case 14: $randNum = 'E'; break;
			case 15: $randNum = 'F'; break;
		}
		$str .= $randNum;
	}
	return $str;
}

$color = randomColor();
echo $color;
// $color1 = randomColor();
// echo $color1;
// $color2 = randomColor();
// echo $color2;
// 每个字的颜色都是随机的，给每个字调用这个函数生成随机色即可
?>
<p style="font-size: 20px; color: <?php echo $color;?>">随机色</p>