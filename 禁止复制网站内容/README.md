五种禁止复制网站内容的方法
第一种方法：
	可以把这段js代码加到你网页上，即可屏蔽鼠标右键菜单、复制粘贴、选中等。
	示例：1.html

第二种方法：
加入以下js代码

<script type="text/javascript">
// oncontextmenu 事件在元素中用户右击鼠标时触发并打开上下文菜单
document.oncontextmenu=new Function("event.returnValue=false"); 
// onselectstart几乎可以用于所有对象，其触发时间为目标对象被开始选中时（即选中动作刚开始，尚未实质性被选中）
document.onselectstart=new Function("event.returnValue=false"); 
</script>
示例：2.html

第三种方法：
在<body>中加入以下代码：

<body oncontextmenu="return false" onselectstart="return false"> 
或 
<body oncontextmenu="event.returnValue=false" onselectstart="event.returnValue=false">

body中加入代码的这种方法有个缺陷就是取决于body的内容，如果body内容较少，从body下方往上选中内容，仍然是可以复制网站的内容的。
示例：3.html

第四种方法：
如果只限制复制，可以在<body>加入以下代码：

<body oncopy="alert('对不起，禁止复制！');return false;"> 
示例：4.html

第五种方法：
禁用Ctrl+C和Ctrl+V，代码：

// 禁用Ctrl+C和Ctrl+V（所有浏览器均支持） 
$(document).keydown(function(e) {
  	if(e.ctrlKey && (e.keyCode == 86 || e.keyCode == 67)) {
    	return false;
  	}
});
示例：5.html