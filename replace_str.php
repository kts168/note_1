<?php
/**
 * 隐藏部分字符串
 * # 此方法多用于手机号码或身份证号、银行卡号的中间部分数字的隐藏
 */
function func_str_replace($str,$replace='*',$start=0,$len=4){
	$length = mb_strlen($str);
	if($length>intval($start+$len)){
		$forword = mb_substr($str,$start,3,'utf-8');
		$end = mb_substr($str,intval($length-$len),null,'utf-8');
	} else {
		$forword = mb_substr($str,0,1,'utf-8');
        $end = mb_substr($str,$length-1,1,'utf-8');
        $len = $length - 2;
	}
	$new_str = $forword;
    for ($i = 0; $i < $len; $i++) {
        $new_str .= $replace;
    }
    $new_str .= $end;
    return $new_str;
}
/**
 * 只保留字符串的首尾字符，中间的部分用*代替（两个字的只显示第一个字）
 * @param string $user_name 姓名
 * @return string 格式化后的姓名
 */
function substr_cut($name){
	$strlen = mb_strlen($name,'utf-8');
	$firstStr = mb_substr($name,0,1,'utf-8');
	$lastStr = mb_substr($name,$strlen-1,1,'utf-8');
	if($strlen == 2){
		$hideStr = str_repeat('*',$strlen - 1);
        $result = $firstStr . $hideStr ;
	} else {
		$hideStr = str_repeat("*", $strlen - 2);
        $result = $firstStr . $hideStr . $lastStr;
	}
	return $result;
}